package stjepan.fragments_weather_app.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import stjepan.fragments_weather_app.FragmentHolderActivity;
import stjepan.fragments_weather_app.R;
import stjepan.fragments_weather_app.adapters.ForecastAdapter;
import stjepan.fragments_weather_app.helpers.RetrofitHelper;
import stjepan.fragments_weather_app.interfaces.API;
import stjepan.fragments_weather_app.models.forecast.ForecastSearchResult;
import stjepan.fragments_weather_app.models.forecast.WeatherInfoList;


/**
 * Created by Stjepan on 16.11.2017..
 */

public class TodayFragment extends Fragment implements Callback<ForecastSearchResult> {
    public static final String TITLE = "Today";

    @BindView(R.id.tvCityNameFragToday) TextView tvCityNameFragToday;
    @BindView(R.id.lvWeatherFragToday) ListView lvWeatherFragToday;

    public TodayFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_today,container,false);
        ButterKnife.bind(this, view);
        initializeUI();
        return view;
    }

    private void initializeUI() {
        String cityName = ((FragmentHolderActivity)getActivity()).getCityName();
        this.tvCityNameFragToday.setText(cityName.toUpperCase());

        Retrofit retrofit = RetrofitHelper.getInstance();

        API api = retrofit.create(API.class);
        Call<ForecastSearchResult> request;
        request = api.getForecastInfo(cityName, API.UNIT, API.API_KEY);
        request.enqueue(this);
    }

    @Override
    public void onResponse(Call<ForecastSearchResult> call, Response<ForecastSearchResult> response) {
        ForecastSearchResult result = response.body();
        removeWhereDateIsNotToday(result);
        ForecastAdapter adapter = new ForecastAdapter(result);
        this.lvWeatherFragToday.setAdapter(adapter);
    }
    private void removeWhereDateIsNotToday(ForecastSearchResult result) {
        List<WeatherInfoList> list = result.getWeatherInfoList();

        Date currentDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = format.format(currentDate);

        //String[] split = result.getDt_txt().split("\\s+");
        //holder.tvDateForecast.setText(split[1] + " h");
        List<WeatherInfoList> forRemoval = new ArrayList<>();

        for (WeatherInfoList listElement : list) {
            String[] split = listElement.getDt_txt().split("\\s+");
            String elementDate = split[0];

            Log.d("ELEMENT", elementDate);
            Log.d("TODAY", dateString);

            if (!elementDate.equals(dateString)){
                forRemoval.add(listElement);
            }
        }
        list.removeAll(forRemoval);


        /*
        for (WeatherInfoList listElement:list) {
            String[] dtArray = listElement.getDt_txt().split("\\s+");
            String hour = dtArray[1];
            String[] hourArray = hour.split(":");
            String formatedDate = hourArray[0] + ":" + hourArray[1] + " h";
            listElement.setmDt_txt(formatedDate);
        }
         */

    }

    @Override
    public void onFailure(Call<ForecastSearchResult> call, Throwable t) {
        Log.e("Fail", t.getMessage());
    }
}
