package stjepan.fragments_weather_app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import stjepan.fragments_weather_app.FragmentHolderActivity;
import stjepan.fragments_weather_app.R;
import stjepan.fragments_weather_app.adapters.WeatherCurrentAdapter;
import stjepan.fragments_weather_app.helpers.RetrofitHelper;
import stjepan.fragments_weather_app.interfaces.API;
import stjepan.fragments_weather_app.models.weatherCurrent.WeatherCurrentSearchResult;

/**
 * Created by Stjepan on 16.11.2017..
 */

public class NowFragment extends Fragment implements Callback<WeatherCurrentSearchResult> {
    public static final String TITLE = "Now";

    @BindView(R.id.tvCityNameFragNow) TextView tvCityNameFragNow;
    @BindView(R.id.lvWeatherFragNow) ListView lvWeatherFragNow;

    public NowFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_now,container,false);
        ButterKnife.bind(this, view);
        initializeUI();
        return view;
    }

    private void initializeUI() {
        String cityName = ((FragmentHolderActivity)getActivity()).getCityName();
        this.tvCityNameFragNow.setText(cityName.toUpperCase());

        Retrofit retrofit = RetrofitHelper.getInstance();

        API api = retrofit.create(API.class);
        Call<WeatherCurrentSearchResult> request;
        request = api.getCurrentWeatherInfo(cityName, API.UNIT, API.API_KEY);
        request.enqueue(this);
    }
    @Override
    public void onResponse(Call<WeatherCurrentSearchResult> call, Response<WeatherCurrentSearchResult> response) {
        WeatherCurrentSearchResult result = response.body();
        WeatherCurrentAdapter adapter = new WeatherCurrentAdapter(result);
        this.lvWeatherFragNow.setAdapter(adapter);
    }

    @Override
    public void onFailure(Call<WeatherCurrentSearchResult> call, Throwable t) {
        Log.e("Fail", t.getMessage());
    }
}
