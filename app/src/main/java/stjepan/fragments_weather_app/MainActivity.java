package stjepan.fragments_weather_app;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    @BindView(R.id.etCityName)
    EditText etCityName;
    @BindView(R.id.btnSearch)
    Button btnSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }
    @OnClick(R.id.btnSearch)
    public void searchCityWeather(){
        String cityName = this.etCityName.getText().toString();

        if (cityName.equals("")){
            Toast.makeText(this, "Bad input", Toast.LENGTH_SHORT).show();
        }
        else{
            //Intent intent = new Intent(this, WeatherActivity.class);
            Intent intent = new Intent(this, FragmentHolderActivity.class);
            intent.putExtra(FragmentHolderActivity.KEY_CITY_NAME, cityName);
            this.startActivity(intent);
        }
    }
}
