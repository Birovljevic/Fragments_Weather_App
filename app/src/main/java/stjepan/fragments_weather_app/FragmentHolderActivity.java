package stjepan.fragments_weather_app;

import android.app.ActionBar;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import stjepan.fragments_weather_app.adapters.ViewPagerAdapter;
import stjepan.fragments_weather_app.fragments.NowFragment;
import stjepan.fragments_weather_app.fragments.TodayFragment;
import stjepan.fragments_weather_app.fragments.WeekFragment;

public class FragmentHolderActivity extends AppCompatActivity {

    public static String CITY_NAME = "";
    public static String KEY_CITY_NAME = "city_name";

    @BindView(R.id.vpPager) ViewPager vpPager;
    @BindView(R.id.tlTabs) TabLayout tlTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);
        ButterKnife.bind(this);
        this.handleExtraData(this.getIntent());
        this.loadFragments();
    }
    public String getCityName(){
        return this.CITY_NAME;
    }
    private void handleExtraData(Intent startingIntent) {
        if (startingIntent != null) {
            if (startingIntent.hasExtra(KEY_CITY_NAME)) {
                this.CITY_NAME = getIntent().getStringExtra(KEY_CITY_NAME);
            }
        }
    }
    private void loadFragments() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.insertFragment(new NowFragment(),NowFragment.TITLE);
        adapter.insertFragment(new TodayFragment(), TodayFragment.TITLE);
        adapter.insertFragment(new WeekFragment(), WeekFragment.TITLE);
        this.vpPager.setAdapter(adapter);
        this.tlTabs.setupWithViewPager(this.vpPager);
    }
}
