package stjepan.fragments_weather_app.interfaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import stjepan.fragments_weather_app.models.forecast.ForecastSearchResult;
import stjepan.fragments_weather_app.models.weatherCurrent.WeatherCurrentSearchResult;

/**
 * Created by Stjepan on 16.11.2017..
 */

public interface API {
    String BASE_URL = "http://api.openweathermap.org/";
    String API_KEY = "5d484613a764056a49d2f5d6a8c401e4";
    String UNIT = "metric";
    String IMAGE_URL = "http://openweathermap.org/img/w/";

    @GET("data/2.5/weather")
    Call<WeatherCurrentSearchResult> getCurrentWeatherInfo(
            @Query("q") String cityName,
            @Query("units") String UNIT,
            @Query("APPID") String API_KEY);

     @GET("data/2.5/forecast")
    Call<ForecastSearchResult> getForecastInfo(
            @Query("q") String cityName,
            @Query("units") String UNIT,
            @Query("APPID") String API_KEY);
}
