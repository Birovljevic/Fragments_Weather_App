package stjepan.fragments_weather_app.models.weatherCurrent;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stjepan on 16.11.2017..
 */

public class CloudsCurrent {
    @SerializedName("all") private String mAll;

    public CloudsCurrent() {
    }

    public CloudsCurrent(String mAll) {
        this.mAll = mAll;
    }

    public String getmAll() {
        return mAll;
    }
}
