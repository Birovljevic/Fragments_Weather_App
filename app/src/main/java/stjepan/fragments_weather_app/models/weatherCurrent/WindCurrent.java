package stjepan.fragments_weather_app.models.weatherCurrent;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stjepan on 16.11.2017..
 */

public class WindCurrent {
    @SerializedName("speed") private Double mSpeed;
    @SerializedName("deg") private Double mDeg;

    public WindCurrent() {
    }

    public WindCurrent(Double mSpeed, Double mDeg) {
        this.mSpeed = mSpeed;
        this.mDeg = mDeg;
    }

    public Double getmSpeed() {
        return mSpeed;
    }

    public Double getmDeg() {
        return mDeg;
    }
}
