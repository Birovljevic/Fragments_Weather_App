package stjepan.fragments_weather_app.models.weatherCurrent;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stjepan on 16.11.2017..
 */

public class SysCurrent {
    @SerializedName("type") private int mType;
    @SerializedName("id") private int mId;
    @SerializedName("message") private Double mMessage;
    @SerializedName("country") private String mCountry;
    @SerializedName("sunrise") private int mSunrise;
    @SerializedName("sunset") private int mSunset;

    public SysCurrent() {
    }

    public SysCurrent(int mType, int mId, Double mMessage, String mCountry, int mSunrise, int mSunset) {
        this.mType = mType;
        this.mId = mId;
        this.mMessage = mMessage;
        this.mCountry = mCountry;
        this.mSunrise = mSunrise;
        this.mSunset = mSunset;
    }

    public int getmType() {
        return mType;
    }

    public int getmId() {
        return mId;
    }

    public Double getmMessage() {
        return mMessage;
    }

    public String getmCountry() {
        return mCountry;
    }

    public int getmSunrise() {
        return mSunrise;
    }

    public int getmSunset() {
        return mSunset;
    }
}
