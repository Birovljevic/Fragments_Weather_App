package stjepan.fragments_weather_app.models.weatherCurrent;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stjepan on 16.11.2017..
 */

public class MainCurrent {
    @SerializedName("temp") private Double mTemp;
    @SerializedName("pressure") private Double mPressure;
    @SerializedName("humidity") private Double mHumidity;
    @SerializedName("temp_min") private Double mTempMin;
    @SerializedName("temp_max") private Double mTempMax;

    public MainCurrent(){

    }

    public MainCurrent(Double mTemp, Double mPressure, Double mHumidity, Double mTempMin, Double mTempMax) {
        this.mTemp = mTemp;
        this.mPressure = mPressure;
        this.mHumidity = mHumidity;
        this.mTempMin = mTempMin;
        this.mTempMax = mTempMax;
    }

    public Double getmTemp() {
        return mTemp;
    }

    public Double getmPressure() {
        return mPressure;
    }

    public Double getmHumidity() {
        return mHumidity;
    }

    public Double getmTempMin() {
        return mTempMin;
    }

    public Double getGetTempMax() {
        return mTempMax;
    }
}
