package stjepan.fragments_weather_app.models.weatherCurrent;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Stjepan on 16.11.2017..
 */

public class WeatherCurrentSearchResult {
    @SerializedName("coord") private CoordCurrent mCordCurrent;
    @SerializedName("weather") private List<WeatherListCurrent> mWeatherListCurrent;
    @SerializedName("base") private String mBaseCurrent;
    @SerializedName("main") private MainCurrent mMainCurrent;
    @SerializedName("visibility") private int mVisibilityCurrent;
    @SerializedName("wind") private WindCurrent mWindCurrent;
    @SerializedName("clouds") private CloudsCurrent mCloudsCurrent;
    @SerializedName("dt") private int mDtCurrent;
    @SerializedName("sys") private SysCurrent mSysCurrent;
    @SerializedName("id") private int mIdCurrent;
    @SerializedName("name") private String mNameCurrent;
    @SerializedName("cod") private int mCodCurrent;

    public WeatherCurrentSearchResult(){

    }
    public WeatherCurrentSearchResult(CoordCurrent mCordCurrent, List<WeatherListCurrent> mWeatherListCurrent, String mBaseCurrent, MainCurrent mMainCurrent, int mVisibilityCurrent, WindCurrent mWindCurrent, CloudsCurrent mCloudsCurrent, int mDtCurrent, SysCurrent mSysCurrent, int mIdCurrent, String mNameCurrent, int mCodCurrent) {
        this.mCordCurrent = mCordCurrent;
        this.mWeatherListCurrent = mWeatherListCurrent;
        this.mBaseCurrent = mBaseCurrent;
        this.mMainCurrent = mMainCurrent;
        this.mVisibilityCurrent = mVisibilityCurrent;
        this.mWindCurrent = mWindCurrent;
        this.mCloudsCurrent = mCloudsCurrent;
        this.mDtCurrent = mDtCurrent;
        this.mSysCurrent = mSysCurrent;
        this.mIdCurrent = mIdCurrent;
        this.mNameCurrent = mNameCurrent;
        this.mCodCurrent = mCodCurrent;
    }

    public CoordCurrent getmCordCurrent() {
        return mCordCurrent;
    }

    public List<WeatherListCurrent> getmWeatherListCurrent() {
        return mWeatherListCurrent;
    }

    public String getmBaseCurrent() {
        return mBaseCurrent;
    }

    public MainCurrent getmMainCurrent() {
        return mMainCurrent;
    }

    public int getmVisibilityCurrent() {
        return mVisibilityCurrent;
    }

    public WindCurrent getmWindCurrent() {
        return mWindCurrent;
    }

    public CloudsCurrent getmCloudsCurrent() {
        return mCloudsCurrent;
    }

    public int getmDtCurrent() {
        return mDtCurrent;
    }

    public SysCurrent getmSysCurrent() {
        return mSysCurrent;
    }

    public int getmIdCurrent() {
        return mIdCurrent;
    }

    public String getmNameCurrent() {
        return mNameCurrent;
    }

    public int getmCodCurrent() {
        return mCodCurrent;
    }
}
