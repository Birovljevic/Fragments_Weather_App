package stjepan.fragments_weather_app.models.weatherCurrent;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stjepan on 16.11.2017..
 */

public class CoordCurrent {
    @SerializedName("lon") private Double mLon;
    @SerializedName("lat") private Double mLat;

    public CoordCurrent(){

    }

    public CoordCurrent(Double mLon, Double mLat) {
        this.mLon = mLon;
        this.mLat = mLat;
    }

    public Double getmLon() {
        return mLon;
    }

    public Double getmLat() {
        return mLat;
    }
}
