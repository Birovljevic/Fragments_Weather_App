package stjepan.fragments_weather_app.models.forecast;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stjepan on 6.11.2017..
 */

public class Sys {

    @SerializedName("pod") private String mPod;

    public Sys(){

    }

    public Sys(String mPod) {

        this.mPod = mPod;
    }

    public String getmPod() {
        return mPod;
    }
}
