package stjepan.fragments_weather_app.models.forecast;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stjepan on 6.11.2017..
 */

public class City {

    @SerializedName("id") private int mId;
    @SerializedName("name") private String mName;
    @SerializedName("coord") private Coord mCoord;
    @SerializedName("country") private String mCountry;

    public City(){

    }

    public City(int mId, String mName, Coord mCoord, String mCountry) {
        this.mId = mId;
        this.mName = mName;
        this.mCoord = mCoord;
        this.mCountry = mCountry;
    }

    public int getmId() {
        return mId;
    }

    public String getmName() {
        return mName;
    }

    public Coord getmCoord() {
        return mCoord;
    }

    public String getmCountry() {
        return mCountry;
    }
}
