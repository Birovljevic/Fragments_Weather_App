package stjepan.fragments_weather_app.models.forecast;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stjepan on 6.11.2017..
 */

public class Weather {

    @SerializedName("id") private int mId;
    @SerializedName("main") private String mMain;
    @SerializedName("description") private String mDescription;
    @SerializedName("icon") private String mIcon;

    //region Constructors
    public Weather(){

    }
    public Weather(int mId, String mMain, String mDescription, String mIcon) {
        this.mId = mId;
        this.mMain = mMain;
        this.mDescription = mDescription;
        this.mIcon = mIcon;
    }

    //endregion

    //region Getters

    public int getmId() {
        return mId;
    }

    public String getmMain() {
        return mMain;
    }

    public String getmDescription() {
        return mDescription;
    }

    public String getmIcon() {
        return mIcon;
    }


    //endregion
}
