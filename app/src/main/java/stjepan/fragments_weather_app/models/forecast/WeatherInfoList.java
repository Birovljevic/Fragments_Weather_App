package stjepan.fragments_weather_app.models.forecast;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Stjepan on 6.11.2017..
 */

public class WeatherInfoList {
    @SerializedName("dt") private int mDt;
    @SerializedName("main") private Main mMain;
    @SerializedName("weather") private List<Weather> mWeather;
    @SerializedName("clouds") private Clouds mClouds;
    @SerializedName("wind") private Wind mWind;
    @SerializedName("rain") private Rain mRain;
    @SerializedName("snow") private Snow mSnow;
    @SerializedName("sys") private Sys mSys;
    @SerializedName("dt_txt") private String mDt_txt;

    //region Constructors
    public WeatherInfoList(){

    }

    public WeatherInfoList(
            int mDt, Main mMain, List<Weather> mWeather, Clouds mClouds, Wind mWind,
            Rain mRain, Snow mSnow, Sys mSys, String mDt_txt) {

        this.mDt = mDt;
        this.mMain = mMain;
        this.mWeather = mWeather;
        this.mClouds = mClouds;
        this.mWind = mWind;
        this.mRain = mRain;
        this.mSnow = mSnow;
        this.mSys = mSys;
        this.mDt_txt = mDt_txt;
    }
    //endregion

    //setter kojim mijenjam polje mDt_txt tako sto mu za objekte kojima popunjavam listView-a fragmenta Week rezem sat i ostavljam samo datum

    public void setmDt_txt(String mDt_txt) {
        this.mDt_txt = mDt_txt;
    }

    //region Getters
    public int getDt() {
        return mDt;
    }

    public Main getMain() {
        return mMain;
    }

    public List<Weather> getWeather() {
        return mWeather;
    }

    public Clouds getClouds() {
        return mClouds;
    }

    public Wind getWind() {
        return mWind;
    }

    public Rain getRain() {
        return mRain;
    }

    public Snow getSnow() {
        return mSnow;
    }

    public Sys getSys() {
        return mSys;
    }

    public String getDt_txt() {
        return mDt_txt;
    }
    //endregion


}
