package stjepan.fragments_weather_app.models.forecast;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stjepan on 6.11.2017..
 */

public class Coord {

    @SerializedName("lat") private double mLat;
    @SerializedName("lon") private double mLon;

    public Coord(){

    }

    public Coord(double mLat, double mLon) {
        this.mLat = mLat;
        this.mLon = mLon;
    }

    public double getmLat() {
        return mLat;
    }

    public double getmLon() {
        return mLon;
    }
}
