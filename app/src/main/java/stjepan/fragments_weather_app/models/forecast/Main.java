package stjepan.fragments_weather_app.models.forecast;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stjepan on 6.11.2017..
 */

public class Main {

    @SerializedName("temp") private double mTemp;
    @SerializedName("temp_min") private double mTempMin;
    @SerializedName("temp_max") private double mTempMax;
    @SerializedName("pressure") private double mPressure;
    @SerializedName("sea_level") private double mSeaLevel;
    @SerializedName("grnd_level") private double mGrndLevel;
    @SerializedName("humidity") private int mHumidity;
    @SerializedName("temp_kf") private double mTempKf;

    //region Constructors
    public Main() {
    }

    public Main(
            double mTemp, double mTempMin, double mTempMax, double mPressure, double mSeaLevel,
            double mGrndLevel, int mHumidity, double mTempKf) {

        this.mTemp = mTemp;
        this.mTempMin = mTempMin;
        this.mTempMax = mTempMax;
        this.mPressure = mPressure;
        this.mSeaLevel = mSeaLevel;
        this.mGrndLevel = mGrndLevel;
        this.mHumidity = mHumidity;
        this.mTempKf = mTempKf;
    }
    //endregion

    //region Getters

    public double getmTemp() {
        return mTemp;
    }

    public double getmTempMin() {
        return mTempMin;
    }

    public double getmTempMax() {
        return mTempMax;
    }

    public double getmPressure() {
        return mPressure;
    }

    public double getmSeaLevel() {
        return mSeaLevel;
    }

    public double getmGrndLevel() {
        return mGrndLevel;
    }

    public int getmHumidity() {
        return mHumidity;
    }

    public double getmTempKf() {
        return mTempKf;
    }
    //endregion
}
