package stjepan.fragments_weather_app.models.forecast;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stjepan on 6.11.2017..
 */

public class Rain {

    @SerializedName("3h") private double m3h;

    public Rain(){

    }

    public Rain(double m3h) {
        this.m3h = m3h;
    }

    public double getM3h() {
        return m3h;
    }
}
