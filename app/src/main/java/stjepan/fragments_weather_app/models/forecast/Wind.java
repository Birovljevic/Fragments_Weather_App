package stjepan.fragments_weather_app.models.forecast;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Stjepan on 6.11.2017..
 */

public class Wind {

    @SerializedName("speed") private double mSpeed;
    @SerializedName("deg")  private double mDeg;

    public Wind(){

    }

    public Wind(double mSpeed, double mDeg) {
        this.mSpeed = mSpeed;
        this.mDeg = mDeg;
    }

    public double getmSpeed() {
        return mSpeed;
    }

    public double getmDeg() {
        return mDeg;
    }
}
