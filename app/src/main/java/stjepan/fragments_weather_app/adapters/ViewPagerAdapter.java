package stjepan.fragments_weather_app.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stray on 21.9.2017..
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

	List<Fragment> mFragmentList;
	List<String> mFragmentTitleList;

	public ViewPagerAdapter(FragmentManager fm) {
		super(fm);
		this.mFragmentList = new ArrayList<>();
		this.mFragmentTitleList = new ArrayList<>();
	}

	@Override
	public Fragment getItem(int position) { return this.mFragmentList.get(position); }

	@Override
	public CharSequence getPageTitle(int position) { return this.mFragmentTitleList.get(position); }

	@Override
	public int getCount() { return this.mFragmentList.size(); }

	public void insertFragment(Fragment fragment, String title) {
		this.mFragmentList.add(fragment);
		this.mFragmentTitleList.add(title);
	}
}
