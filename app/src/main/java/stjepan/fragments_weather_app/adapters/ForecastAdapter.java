package stjepan.fragments_weather_app.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import stjepan.fragments_weather_app.R;
import stjepan.fragments_weather_app.interfaces.API;
import stjepan.fragments_weather_app.models.forecast.ForecastSearchResult;
import stjepan.fragments_weather_app.models.forecast.WeatherInfoList;


/**
 * Created by Stjepan on 6.11.2017..
 */

public class ForecastAdapter extends BaseAdapter {

    ForecastSearchResult mWeather;

    public ForecastAdapter(ForecastSearchResult mWeather) {
        this.mWeather = mWeather;
        formatDateTime();
    }

    private void formatDateTime() {
        List<WeatherInfoList> list = this.mWeather.getWeatherInfoList();

        for (WeatherInfoList listElement:list) {
            String[] dtArray = listElement.getDt_txt().split("\\s+");
            String date = dtArray[0];
            String hours = dtArray[1];
            String[] dateArray = date.split("-");
            String[] hoursArray = hours.split(":");
            String formatedDateTime = dateArray[2] + "." + dateArray[1] + "." + dateArray[0] + "." + "\n\t" + hoursArray[0] + ":" + hoursArray[1] + " h";
            listElement.setmDt_txt(formatedDateTime);
        }
    }

    @Override
    public int getCount() {
        return this.mWeather.getWeatherInfoList().size();
    }

    @Override
    public Object getItem(int position) {
        return this.mWeather.getWeatherInfoList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ForecastHolder holder;
        if (convertView == null){
            convertView= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_forecast, parent, false);
            holder = new ForecastHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (ForecastHolder) convertView.getTag();
        }

        WeatherInfoList result = this.mWeather.getWeatherInfoList().get(position);

        holder.tvDateForecast.setText(result.getDt_txt());
        holder.tvTemperatureMinForecast.setText("Min Temp: " + String.valueOf(result.getMain().getmTempMax()) + " °C");
        holder.tvTemperatureMaxForecast.setText("Max Temp: " + String.valueOf(result.getMain().getmTempMin()) + " °C");

        for (stjepan.fragments_weather_app.models.forecast.Weather Weather : result.getWeather()) {

            holder.tvMainAndDescriptionForecast.setText(Weather.getmMain() + ": " + Weather.getmDescription());

            Picasso.with(parent.getContext())
                    .load(API.IMAGE_URL + Weather.getmIcon() + ".png")
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.image_not_found)
                    .into(holder.ivWeatherIconForecast);
        }

        return convertView;
}
    static class ForecastHolder
    {
        @BindView(R.id.tvDateForecast) TextView tvDateForecast;
        @BindView(R.id.tvTemperatureMinForecast) TextView tvTemperatureMinForecast;
        @BindView(R.id.tvTemperatureMaxForecast) TextView tvTemperatureMaxForecast;
        @BindView(R.id.ivWeatherIconForecast) ImageView ivWeatherIconForecast;
        // main + description
        @BindView(R.id.tvMainAndDescriptionForecast) TextView tvMainAndDescriptionForecast;

        public ForecastHolder(View view)
        {
            ButterKnife.bind(this, view);
        }
    }
}
