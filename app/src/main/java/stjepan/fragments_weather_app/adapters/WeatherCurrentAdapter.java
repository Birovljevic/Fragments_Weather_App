package stjepan.fragments_weather_app.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import stjepan.fragments_weather_app.R;
import stjepan.fragments_weather_app.interfaces.API;
import stjepan.fragments_weather_app.models.weatherCurrent.MainCurrent;
import stjepan.fragments_weather_app.models.weatherCurrent.WeatherCurrentSearchResult;

/**
 * Created by Stjepan on 16.11.2017..
 */

public class WeatherCurrentAdapter extends BaseAdapter{
    WeatherCurrentSearchResult mWeatherCurrentSearchResult;

    public WeatherCurrentAdapter(WeatherCurrentSearchResult mWeatherCurrentSearchResult) {
        this.mWeatherCurrentSearchResult = mWeatherCurrentSearchResult;
    }


    @Override
    public int getCount() {
        return this.mWeatherCurrentSearchResult.getmWeatherListCurrent().size();
    }

    @Override
    public Object getItem(int i) {
        return this.mWeatherCurrentSearchResult.getmWeatherListCurrent().get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        WeatherHolder holder;
        if (convertView == null){
            convertView= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_weather_current, parent, false);
            holder = new WeatherHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (WeatherHolder) convertView.getTag();
        }

        MainCurrent result = this.mWeatherCurrentSearchResult.getmMainCurrent();
        //WeatherInfoList result = this.mWeather.getWeatherInfoList().get(position);

        Date currentDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = format.format(currentDate);

        holder.tvDate.setText(dateString.replace("-", ".") + ".");
        holder.tvTemperature.setText(String.valueOf(result.getmTemp())+ " °C");
        holder.tvTemperatureMin.setText("Min Temp: " + String.valueOf(result.getmTempMin()) + " °C");
        holder.tvTemperatureMax.setText("Max Temp: " + String.valueOf(result.getGetTempMax()) + " C");


        for (stjepan.fragments_weather_app.models.weatherCurrent.WeatherListCurrent weather : this.mWeatherCurrentSearchResult.getmWeatherListCurrent()){
            Picasso.with(parent.getContext())
                    .load((API.IMAGE_URL + weather.getmIcon() + ".png"))
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.image_not_found)
                    .into(holder.ivWeatherIcon);
        }
        return convertView;
    }
    static class WeatherHolder
    {
        @BindView(R.id.tvDate) TextView tvDate;
        @BindView(R.id.tvTemperature) TextView tvTemperature;
        @BindView(R.id.tvTemperatureMin) TextView tvTemperatureMin;
        @BindView(R.id.tvTemperatureMax) TextView tvTemperatureMax;
        @BindView(R.id.ivWeatherIcon) ImageView ivWeatherIcon;

        public WeatherHolder(View view)
        {
            ButterKnife.bind(this, view);
        }
    }
}
