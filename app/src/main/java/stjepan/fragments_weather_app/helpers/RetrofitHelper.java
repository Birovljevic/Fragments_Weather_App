package stjepan.fragments_weather_app.helpers;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import stjepan.fragments_weather_app.interfaces.API;
import stjepan.fragments_weather_app.models.weatherCurrent.WeatherCurrentSearchResult;

/**
 * Created by Stjepan on 16.11.2017..
 */

public class RetrofitHelper {

    private static Retrofit mRetroift;

    /*
    private RetrofitHelper(){
        this.mRetroift = new Retrofit.Builder()
                .baseUrl(API.BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
     */

    public static synchronized Retrofit getInstance(){
        if (mRetroift == null){
            mRetroift = new Retrofit.Builder()
                    .baseUrl(API.BASE_URL)
                    .client(new OkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
            return mRetroift;
    }
}
